import { Component } from '@angular/core';
import {RegistrarService} from '../../services/registrar.service';
import {NavController, NavParams ,AlertController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-formulario2',
  templateUrl: 'formulario2.html'
})
export class Formulario2Component {

  text: string;
  private formulario = {};
  private formulario2 : FormGroup;

  constructor(_registrar:RegistrarService,
    public navC:NavController,
    private NavParams:NavParams,
    public alertCtrl: AlertController,
    private formBuilder: FormBuilder) {

    this.formulario =  this.NavParams.get("form");
    this.formulario2 = this.formBuilder.group({
      pais: [''],
      localidad: [''],
      ciudad: [''],
      direccion: [''],
      cod: [''],
      telefono:['']
  });

  }



    showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Datos',
      subTitle: 'Nombre:'+this.NavParams.data.form.nombre+
                ' apellido:'+this.NavParams.data.form.apellido+
                ' Genero:'+this.NavParams.data.form.genero+
                ' Fecha:'+this.NavParams.data.form.fecha+
                ' Pais:'+this.formulario2.value.pais+
                ' Localidad:'+this.formulario2.value.localidad+
                ' Ciudad:'+this.formulario2.value.ciudad+
                ' Direccion:'+this.formulario2.value.direccion+
                ' Telefono:'+this.formulario2.value.cod+this.formulario2.value.telefono,
      buttons: ['OK']
    });
    alert.present();
  }


}
