import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Formulario2Component} from '../formulario2/formulario2';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-formulario1',
  templateUrl: 'formulario1.html'
})
export class Formulario1Component {


  private formulario : FormGroup;

  constructor( private navC:NavController,private formBuilder: FormBuilder) {
    this.formulario = this.formBuilder.group({
      nombre: [''],
      apellido: [''],
      genero: [''],
      fecha: ['']
  });

  }

   irFormulario2(form=this.formulario.value){
     console.log(this.formulario.value)
     this.navC.push(Formulario2Component,{form});
   }



}
